# Language redirect

A cookie-free solution for language redirects based on the HTTP_ACCEPT_LANGUAGE header for TYPO3 CMS.


## How does it work?

When you call a URL without the language parameter `L`, the extension will map the HTTP_ACCEPT_LANGUAGE header to the installed system languages and redirect to it. Should also work with all available url encryption extensions (e.g. realurl).


## Installation

Simply install the extension via composer 

```sh
composer req biedert/language-redirect
```

or download it via th TYPO3 Extension Repository: https://extensions.typo3.org/extension/language_redirect/

## Configuration

To map the language codes to the system languages, simply list them in the `browserLanguageMatch` setup.

In case the HTTP_ACCEPT_LANGUAGE header doesn't match any system language, the extension will redirect to a fallback language uid defined in the extension config (default is 0).

## Notice 
To make the extension work, make sure you don't have a `defaultGetVars` setting for the language parameter `L`:

```typo3_typoscript
config.defaultGetVars.L >
```
