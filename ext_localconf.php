<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Biedert.' . $_EXTKEY,
    'LanguageRedirect',
    array(
        'LanguageRedirect' => 'redirect',
    ),
    // non-cacheable actions
    array(
        'LanguageRedirect' => 'redirect',
    )
);
