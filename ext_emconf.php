<?php

$EM_CONF[$_EXTKEY] = array(
    'title'            => 'Language Redirect',
    'description'      => '',
    'category'         => 'plugin',
    'author'           => 'Patrick Biedert',
    'author_email'     => 'patrick.biedert@gmx.de',
    'state'            => 'stable',
    'internal'         => '',
    'uploadfolder'     => '0',
    'createDirs'       => '',
    'clearCacheOnLoad' => 0,
    'version'          => '2.0.2',
    'constraints'      => array(
        'depends'   => array(
            'typo3' => '8.7.0-8.7.99',
        ),
        'conflicts' => array(),
        'suggests'  => array(),
    ),
    'autoload'         => array(
        'classmap' => array('Classes')
    ),
);
