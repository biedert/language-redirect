<?php

namespace Biedert\LanguageRedirect\Controller;

/***************************************************************
 *  Copyright notice
 *  (c) 2019 Patrick Biedert <patrick.biedert@ueberbit.de>, UEBERBIT GmbH
 *  All rights reserved
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Frontend\Page\PageRepository;

class LanguageRedirectController extends ActionController
{

    const DEBUG = false;

    /**
     * @var array
     */
    protected $accepted = [];

    /**
     * @var array
     */
    protected $available = [];

    /**
     * @var array
     */
    protected $matches;

    /**
     * @var int
     */
    protected $fallbackLanguageUid = 0;

    /**
     * @var array
     */
    protected $queryBuilder = [];


    /**
     * parse list of comma separated language tags and sort it by the quality value
     * http://stackoverflow.com/a/3771447
     *
     * @param \string $languageList
     *
     * @return array
     */
    protected function parseLanguageList($languageList)
    {
        $languages = [];
        foreach (GeneralUtility::trimExplode(',', $languageList) as $languageRange) {
            if (preg_match('/(\*|[a-zA-Z0-9]{1,8}(?:-[a-zA-Z0-9]{1,8})*)(?:\s*;\s*q\s*=\s*(0(?:\.\d{0,3})|1(?:\.0{0,3})))?/', trim($languageRange), $match)) {
                if (!isset($match[2])) {
                    $match[2] = 1;
                } else {
                    $match[2] = (string)floatval($match[2]);
                }
                if (!isset($languages[$match[2]])) {
                    $languages[$match[2]] = [];
                }
                $languages[$match[2]][] = strtolower($match[1]);
            }
        }
        krsort($languages);
        return $languages;
    }


    /**
     * compare two parsed arrays of language tags and find the matches
     *
     * @param array $accepted
     * @param array $available
     *
     * @return array
     */
    function findMatches($accepted, $available)
    {
        $matches = array();
        $any     = false;
        foreach ($accepted as $acceptedQuality => $acceptedValues) {
            $acceptedQuality = floatval($acceptedQuality);
            if ($acceptedQuality === 0.0) {
                continue;
            }
            foreach ($acceptedValues as $acceptedValue) {
                if ($acceptedValue === '*') {
                    $any = true;
                }
                foreach ($available as $availableValues) {
                    if (in_array($acceptedValue, $availableValues)) {
                        $matches[(string)$acceptedQuality][] = $acceptedValue;
                    }
                }
            }
        }
        if (count($matches) === 0 && $any) {
            $matches = $available;
        }
        krsort($matches);
        return $matches;
    }


    /**
     * sets up required variables
     *
     * @return void
     */
    protected function setupLanguageRedirect()
    {

        if((int) $this->settings['fallbackLanguageUid']) {
            $this->fallbackLanguageUid =  (int) $this->settings['fallbackLanguageUid'];
        }

        foreach ($this->settings['browserLanguageMatch'] as $sysLanguageUid => $languageCodes) {
            $this->available[(int) $sysLanguageUid] = GeneralUtility::trimExplode(',', $languageCodes);
        }

        $this->accepted = $this->parseLanguageList($_SERVER['HTTP_ACCEPT_LANGUAGE']);
        $this->matches  = $this->findMatches($this->accepted, $this->available);

        if (self::DEBUG) {
            DebuggerUtility::var_dump($_SERVER['HTTP_ACCEPT_LANGUAGE']);
            DebuggerUtility::var_dump($this->accepted, 'Accepted Languages');
            DebuggerUtility::var_dump($this->available, 'Available Languages');
            DebuggerUtility::var_dump($this->matches, 'Matches');
        }
    }


    /**
     * @param string $language
     *
     * @return int
     */
    protected function getTargetLanguageUid($language)
    {
        foreach ($this->available as $key => $value) {
            if (in_array($language, $value)) {
                return (int)$key;
            }
        }
        return $this->fallbackLanguageUid;
    }


    /**
     * @param string $table
     *
     * @return QueryBuilder
     */
    protected function getQueryBuilderForTable($table)
    {
        if (!isset($this->queryBuilder[$table]) || !($this->queryBuilder[$table] instanceof QueryBuilder)) {
            $this->queryBuilder[$table] = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($table);
            $this->queryBuilder[$table]
                ->select('*')
                ->from($table);
        }
        return $this->queryBuilder[$table];
    }


    /**
     * @return string
     */
    public function redirectAction()
    {
        $currentLanguageUid = GeneralUtility::_GP('L');

        if (!is_null($currentLanguageUid)) {
            return '';
        }

        $this->setupLanguageRedirect();

        if (is_array($this->matches) && count($this->matches)) {

            $targetLanguageUid = $this->getTargetLanguageUid(array_shift(array_shift($this->matches)));
            $targetPageUid     = $GLOBALS['TSFE']->id;

            if ($targetLanguageUid > 0) {
                /* @var QueryBuilder $queryBuilder */
                $queryBuilder        = $this->getQueryBuilderForTable('pages_language_overlay');
                $pageLanguageOverlay = $queryBuilder
                    ->where(
                        $queryBuilder->expr()->eq('pid', $queryBuilder->createNamedParameter($targetPageUid, \PDO::PARAM_INT)),
                        $queryBuilder->expr()->eq('sys_language_uid', $queryBuilder->createNamedParameter($targetLanguageUid, \PDO::PARAM_INT))
                    )
                    ->execute()->fetchAll()[0];

                if (is_array($pageLanguageOverlay)) {
                    if ($pageLanguageOverlay['doktype'] === PageRepository::DOKTYPE_SHORTCUT) {
                        switch ($pageLanguageOverlay['shortcut_mode']) {
                            case PageRepository::SHORTCUT_MODE_FIRST_SUBPAGE:
                                $queryBuilder = $this->getQueryBuilderForTable('pages');
                                $menu         = $queryBuilder
                                    ->where(
                                        $queryBuilder->expr()->eq('pid', $queryBuilder->createNamedParameter($pageLanguageOverlay['shortcut'] ?: $pageLanguageOverlay['uid'], \PDO::PARAM_INT))
                                    )
                                    ->execute()->fetchAll();
                                if (!count($menu)) {
                                    $targetPageUid = $pageLanguageOverlay['shortcut'] ?: $pageLanguageOverlay['uid'];
                                } else {
                                    $targetPageUid = array_shift($menu)['uid'];
                                }
                                break;
                            case PageRepository::SHORTCUT_MODE_RANDOM_SUBPAGE:
                                $queryBuilder = $this->getQueryBuilderForTable('pages');
                                $menu         = $queryBuilder
                                    ->where(
                                        $queryBuilder->expr()->eq('pid', $queryBuilder->createNamedParameter($pageLanguageOverlay['shortcut'] ?: $pageLanguageOverlay['uid'], \PDO::PARAM_INT))
                                    )
                                    ->execute()->fetchAll();
                                if (!count($menu)) {
                                    $targetPageUid = $pageLanguageOverlay['shortcut'] ?: $pageLanguageOverlay['uid'];
                                } else {
                                    if (count($menu) == 1) {
                                        $targetPageUid = array_shift($menu)['uid'];
                                    } else {
                                        $targetPageUid = array_values($menu)[rand(0, count($menu) - 1)]['uid'];
                                    }
                                }
                                break;
                            case PageRepository::SHORTCUT_MODE_PARENT_PAGE:
                                $queryBuilder = $this->getQueryBuilderForTable('pages');
                                $page         = $queryBuilder
                                    ->where(
                                        $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($pageLanguageOverlay['shortcut'] ?: $pageLanguageOverlay['pid'], \PDO::PARAM_INT))
                                    )
                                    ->execute()->fetchAll()[0];
                                $targetPageUid = $page['pid'] ? $page['pid'] : $page['uid'];
                                break;
                            default:
                                $targetPageUid = $pageLanguageOverlay['shortcut'];
                                break;
                        }
                    }
                } else {
                    // todo: if first matching translation doesn't exist on page, try next language match
                    $targetLanguageUid = 0;
                }

                if ($targetPageUid != $GLOBALS['TSFE']->id) {
                    $queryBuilder = $this->getQueryBuilderForTable('pages_language_overlay');
                    if (!$queryBuilder
                        ->where(
                            $queryBuilder->expr()->eq('pid', $queryBuilder->createNamedParameter($targetPageUid, \PDO::PARAM_INT)),
                            $queryBuilder->expr()->eq('sys_language_uid', $queryBuilder->createNamedParameter($targetLanguageUid, \PDO::PARAM_INT))
                        )->execute()->fetchAll()[0]) {
                        $targetLanguageUid = $this->fallbackLanguageUid;
                    }
                }
            }

            $url = $this->uriBuilder->reset()->setTargetPageUid($targetPageUid)->setArguments(array('L' => $targetLanguageUid))->build();

            if (self::DEBUG) {
                DebuggerUtility::var_dump($targetLanguageUid, 'Target language uid');
                DebuggerUtility::var_dump($targetPageUid, 'Target page uid');
            }
        } else {
            $url = $this->uriBuilder->reset()->setTargetPageUid($GLOBALS['TSFE']->id)->setArguments(array('L' => $this->fallbackLanguageUid))->build();
        }

        if (self::DEBUG) {
            DebuggerUtility::var_dump($url, 'Redirect Url');
            exit;
        }

        $this->redirectToURI($url);
    }
}
